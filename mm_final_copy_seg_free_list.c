/*
 ******************************************************************************
 *                                   mm.c                                     *
 *           64-bit struct-based implicit free list memory allocator          *
 *                  15-213: Introduction to Computer Systems                  *
 *                                                                            *
 *  ************************************************************************  *
 *                      Assumptions made in this lab:                         *
 *                      1) Memory is word addressed                           *
 *                      2) Words are int-sized                                *
 *                      3) Allocations are double-word aligned                * 
 *                                                                            *
 *                        Segregated Explicit Free                            *
 *                               First Fit                                    *
 *                         -Address index blocks                              *
 *                                                                            *
 *  ************************************************************************  *
 *  ** ADVICE FOR STUDENTS. **                                                *
 *  Step 0: Please read the writeup!                                          *
 *  Step 1: Write your heap checker. Write. Heap. checker.                    *
 *  Step 2: Place your contracts / debugging assert statements.               *
 *  Good luck, and have fun!                                                  *
 *                                                                            *
 ******************************************************************************
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <assert.h>
#include <stddef.h>
#include <unistd.h>
#include <inttypes.h>

#include "mm.h"
#include "memlib.h"

/* Do not change the following! */

#ifdef DRIVER
/* create aliases for driver tests */
#define malloc mm_malloc
#define free mm_free
#define realloc mm_realloc
#define calloc mm_calloc
#define memset mem_memset
#define memcpy mem_memcpy
#endif /* def DRIVER */

/* You can change anything from here onward */

/*
 * If DEBUG is defined (such as when running mdriver-dbg), these macros
 * are enabled. You can use them to print debugging output and to check
 * contracts only in debug mode.
 *
 * Only debugging macros with names beginning "dbg_" are allowed.
 * You may not define any other macros having arguments.
 */
#ifdef DEBUG
/* When DEBUG is defined, these form aliases to useful functions */
#define dbg_printf(...)     printf(__VA_ARGS__)
#define dbg_requires(expr)  assert(expr)
#define dbg_assert(expr)    assert(expr)
#define dbg_ensures(expr)   assert(expr)
#define dbg_printheap(...)  print_heap(__VA_ARGS__)
#else
/* When DEBUG is not defined, no code gets generated for these */
/* The sizeof() hack is used to avoid "unused variable" warnings */
#define dbg_printf(...)     (sizeof(__VA_ARGS__), -1)
#define dbg_requires(expr)  (sizeof(expr), 1)
#define dbg_assert(expr)    (sizeof(expr), 1)
#define dbg_ensures(expr)   (sizeof(expr), 1)
#define dbg_printheap(...)  ((void) sizeof(__VA_ARGS__))
#endif


/* Basic constants */

typedef uint64_t word_t;

// Word and header size (bytes)
static const size_t wsize = sizeof(word_t);

// Double word size (bytes)
static const size_t dsize = 2 * wsize;

// Minimum block size (bytes)
static const size_t min_block_size = 2 * dsize;

// TODO: explain what chunksize is
// (Must be divisible by dsize)
static const size_t chunksize = (1L << 12);

// alloc_mask is used to perform bit-wise & opertation to retrieve allocation
// status regarding the current block
static const word_t alloc_mask = 0x1UL;

// neighbor alloc mask is used to acquire allocation status of the prev and 
// next consecutive block
// static const word_t neighbor_alloc_mask = 0x2UL;

// TODO: explain what size_mask is
static const word_t size_mask = ~(word_t)0xF;


/* Represents the header and payload of one block in the heap */
typedef struct block
{
    /* Header contains size + allocation flag */
    word_t header;

    /*
     * WARNING: A zero-length array must be the last element in a struct, so
     * there should not be any struct fields after it. For this lab, we will
     * allow you to include a zero-length array in a union, as long as the
     * union is the last field in its containing struct. However, this is
     * compiler-specific behavior and should be avoided in general.
     *dbg_ensures(!get_alloc(block));
     * WARNING: DO NOT cast this pointer to/from other types! Instead, you
     * should use a union to alias this zero-length array with another struct,
     * in order to store additional types of data in the payload memory.
     */
    char payload[0];

    /*
     * TODO: delete or replace this comment once you've thdvaought about it.
     * Why can't we declare the block footer here as part of the struct?
     * Why do we even have footers -- will the code work fine without them?
     * which functions actually use the data contained in footers?
     */
} block_t;

/* Free Block */
typedef struct free_block
{
    word_t header;
    struct free_block * next;
    struct free_block * prev;
} free_t;

typedef struct seg_list_header
{
    struct free_block * next;
} seg_h;

/* Global variables */

// Pointer to first block
static const int LIST_SIZE = 20;
static block_t *heap_start = NULL;
static seg_h *seg_list_ptr;
static const uint64_t seglist[LIST_SIZE] = {4UL, 6UL, 8UL, 10UL, 18UL, 26UL, 34UL, 50UL, 66UL,
  98UL, 130UL, 258UL, 386UL, 514UL, 770UL, 1026UL, 1538UL, 2050UL, 4098UL, 4100UL};

/* Function prototypes for internal helper routines */

bool mm_checkheap(int lineno);

static block_t *extend_heap(size_t size);
static block_t *find_fit(size_t asize);
static block_t *coalesce_block(block_t *block);
static void split_block(block_t *block, size_t asize);

static size_t max(size_t x, size_t y);
static size_t min(size_t x, size_t y);
static size_t round_up(size_t size, size_t n);
static word_t pack(size_t size, bool alloc);

static size_t extract_size(word_t header);
static size_t get_size(block_t *block);
static size_t get_payload_size(block_t *block);

static bool extract_alloc(word_t header);
static bool get_alloc(block_t *block);

static void write_header(block_t *block, size_t size, bool alloc);
static void write_footer(block_t *block, size_t size, bool alloc);

static block_t *payload_to_header(void *bp);
static void *header_to_payload(block_t *block);
static word_t *header_to_footer(block_t *block);

static block_t *find_next(block_t *block);
static word_t *find_prev_footer(block_t *block);
static block_t *find_prev(block_t *block);
static void insert(free_t *free_b);
static void remove_from_free(free_t *free_b, int root_index);
void traverse_free();

/*
 * <What does this function do?>
 * <What are the function's arguments?>
 * <What is the function's return value?>
 * <Are there any preconditions or postconditions?>
 */
bool mm_init(void)
{
    heap_start = NULL;
    seg_list_ptr = NULL;
    seg_h *seg_header = NULL;
    
    // Load segregated list headers into the heap
    seg_list_ptr = mem_sbrk(LIST_SIZE * wsize);
    for (int i = 0; i < LIST_SIZE; i++) {
        seg_header = seg_list_ptr + i;
        seg_header->next = NULL;
    }

    // Create the initial empty heap
    word_t *start = (word_t *) (mem_sbrk(2 * wsize));

    if (start == (void *)-1)
    {
        return false;
    }
    start[0] = pack(0UL, true);  // Heap prologue (block footer)
    start[1] = pack(0UL, true);  // Heap epilogue (block header)

    // Heap starts with first "block header", currently the epilogue
    heap_start = (block_t *) &(start[1]);
    
    // Extend the empty heap with a free block of chunksize bytes
    if (extend_heap(chunksize) == NULL)
    {
        return false;
    }

    dbg_requires(mm_checkheap(__LINE__));
    return true;
}


/*
 * <What does this function do?>
 * <What are the function's arguments?>
 * <What is the function's return value?>
 * <Are there any preconditions or postconditions?>
 */
void *malloc(size_t size)
{
    dbg_printf("            MALLOC IS CALLED\n");
    dbg_requires(mm_checkheap(__LINE__));

    size_t asize;      // Adjusted block size
    size_t extendsize; // Amount to extend heap if no fit is found
    block_t *block;
    void *bp = NULL;
    

    if (heap_start == NULL) // Initialize heap if it isn't initialized
    {
        mm_init();
    }

    if (size == 0) // Ignore spurious request
    {
        dbg_ensures(mm_checkheap(__LINE__));
        return bp;
    }

    // Adjust block size to include overhead and to meet alignment requirements
    asize = round_up(size + dsize, dsize);
    dbg_printf("Requesting size is %zu\n", asize);

    // Search the free list for a fit
    block = find_fit(asize);

    // If no fit is found, request more memory, and then and place the block
    if (block == NULL)
    {
        dbg_printf("Extend Heap is called\n");
        // Always request at least chunksize
        extendsize = max(asize, chunksize);
        block = extend_heap(extendsize);
        if (block == NULL) // extend_heap returns an error
        {
            return bp;
        }
    }

    // The block should be marked as free
    dbg_assert(!get_alloc(block));

    // Mark block as allocated
    size_t block_size = get_size(block);
    dbg_printf("block_size: %zu\n", block_size);
    write_header(block, block_size, true);
    write_footer(block, block_size, true);

    // Try to split the block if too large
    split_block(block, asize);

    bp = header_to_payload(block);

    /* TODO: Can you write a postcondition about the alignment of bp? */
    dbg_ensures(mm_checkheap(__LINE__));
    return bp;
}


/*
 * <What does this function do?>
 * <What are the function's arguments?>
 * <What is the function's return value?>
 * <Are there any preconditions or postconditions?>
 */
void free(void *bp)
{
    dbg_printf("            FREE IS CALLED\n");
    dbg_printf("The memory being freed is %p\n", bp);
    dbg_requires(mm_checkheap(__LINE__));

    if (bp == NULL)
    {
        return;
    }

    block_t *block = payload_to_header(bp);
    size_t size = get_size(block);

    // The block should be marked as allocated
    dbg_assert(get_alloc(block));

    // Mark the block as free
    write_header(block, size, false);
    write_footer(block, size, false);

    // Try to coalesce the block with its neighbors
    // LIFO free list is maintained by coalesce
    block = coalesce_block(block);

    dbg_ensures(mm_checkheap(__LINE__));
}


/*
 * <What does this function do?>
 * <What are the function's arguments?>
 * <What is the function's return value?>
 * <Are there any preconditions or postconditions?>
 */
void *realloc(void *ptr, size_t size)
{
    dbg_printf("            REALLOC IS CALLED\n");
    block_t *block = payload_to_header(ptr);
    size_t copysize;
    void *newptr;

    // If size == 0, then free block and return NULL
    if (size == 0)
    {
        free(ptr);
        return NULL;
    }

    // If ptr is NULL, then equivalent to malloc
    if (ptr == NULL)
    {
        return malloc(size);
    }

    // Otherwise, proceed with reallocation
    newptr = malloc(size);

    // If malloc fails, the original block is left untouched
    if (newptr == NULL)
    {
        return NULL;
    }

    // Copy the old data
    copysize = get_payload_size(block); // gets size of old payload
    if (size < copysize)
    {
        copysize = size;
    }
    memcpy(newptr, ptr, copysize);

    // Free the old block
    free(ptr);

    return newptr;
}


/*
 * <What does this function do?>
 * <What are the function's arguments?>
 * <What is the function's return value?>
 * <Are there any preconditions or postconditions?>
 */
void *calloc(size_t elements, size_t size)
{
    void *bp;
    size_t asize = elements * size;

    if (asize/elements != size)
    {
        // Multiplication overflowed
        return NULL;
    }

    bp = malloc(asize);
    if (bp == NULL)
    {
        return NULL;
    }

    // Initialize all bits to 0
    memset(bp, 0, asize);

    return bp;
}


/******** The remaining content below are helper and debug routines ********/

/*
 * <What does this function do?>
 * <What are the function's arguments?>
 * <What is the function's return value?>
 * <Are there any preconditions or postconditions?>
 */
static block_t *extend_heap(size_t size)
{
    void *bp;

    // Allocate an even number of words to maintain alignment
    size = round_up(size, dsize);
    if ((bp = mem_sbrk(size)) == (void *)-1)
    {
        return NULL;
    }

    // Initialize free block header/footer
    block_t *block = payload_to_header(bp);
    write_header(block, size, false);
    write_footer(block, size, false);

    // Create new epilogue header
    block_t *block_next = find_next(block);
    write_header(block_next, 0, true);

    insert((free_t *)block);

    // DESIGNCHOICE: no need to immediately coalesce block
    // block = coalesce_block(block);
    dbg_printf("Finished Coalescing\n");
    
    return block;
}


/*
 * <What does this function do?>
 * <What are the function's arguments?>
 * <What is the function's return value?>
 * <Are there any preconditions or postconditions?>
 * 
 * YOU ONLY COALESCE WHEN FREE or ALLOCATING MORE HEAP
 * 
 */
static block_t *coalesce_block(block_t *block)
{
    dbg_requires(!get_alloc(block));

    size_t size = get_size(block);

    /*
     * TODO: delete or replace this comment once you've thought about it.
     * Think about how we find the prev and next blocks. What information
     * do we need to have about the heap in order to do this? Why doesn't
     * "bool prev_alloc = get_alloc(block_prev)" work properly?
     */

    block_t *block_next = find_next(block);
    block_t *block_prev = find_prev(block);

    // Always works since we have prologue and epilogue footers, no null pointer
    bool prev_alloc = extract_alloc(*find_prev_footer(block));
    bool next_alloc = get_alloc(block_next);
    
    if (prev_alloc && next_alloc)              // Case 1 : both allocated
    {
        // do nothing
    }

    else if (prev_alloc && !next_alloc)        // Case 2 : next not allocated
    {
        size += get_size(block_next);
        remove_from_free((free_t *)block_next, -1);
        write_header(block, size, false);
        write_footer(block, size, false);
    }

    else if (!prev_alloc && next_alloc)        // Case 3 : prev not allocated
    {
        size += get_size(block_prev);
        remove_from_free((free_t *)block_prev, -1);
        write_header(block_prev, size, false);
        write_footer(block_prev, size, false);
        block = block_prev;
    }

    else                                        // Case 4 : neither allocated
    {
        remove_from_free((free_t *)block_next, -1);
        remove_from_free((free_t *)block_prev, -1);
        size += get_size(block_next) + get_size(block_prev);
        write_header(block_prev, size, false);
        write_footer(block_prev, size, false);
        block = block_prev;
    }
    // Connect the newly free block to free_root
    
    dbg_ensures(!get_alloc(block));
    insert((free_t *)block);
    dbg_ensures(mm_checkheap(__LINE__));
    /* TODO: Can you write a postcondition about get_size(block)? */

    return block;
}


/*
 * <What does this do?>
 * <What are the function's arguments?>
 * <What is the function's return value?>
 * <Are there any preconditions or postconditions?>
 */
static void split_block(block_t *block, size_t asize)
{
    dbg_requires(get_alloc(block));
    /* TODO: Can you write a precondition about the value of asize? */
    size_t block_size = get_size(block);
    size_t s = block_size;
    s >>= 3;
    int i;
    for (i = 0; s > seglist[i] && i < LIST_SIZE - 1; i++) 
    {
        ;
    }

    if ((block_size - asize) >= min_block_size)
    {
        block_t *block_next;
        write_header(block, asize, true);
        write_footer(block, asize, true);

        block_next = find_next(block);
        write_header(block_next, block_size - asize, false);
        write_footer(block_next, block_size - asize, false);

        dbg_printf("A block is to be removed from seg list header %d\n", i);
        remove_from_free((free_t *)block, i);
        insert((free_t *)block_next);
    } else {
        remove_from_free((free_t *)block, i);
    }

    dbg_ensures(get_alloc(block));
    dbg_ensures(mm_checkheap(__LINE__));
}

/*
 * <What does this function do?>
 * <What are the function's arguments?>
 * <What is the function's return value?>
 * <Are there any preconditions or postconditions?>
 */
static block_t *find_fit(size_t asize)
{
    free_t *block;
    free_t *head;
    size_t s = asize;
    int i;

    s >>= 3;
    for (i = 0; s > seglist[i] && i < LIST_SIZE - 1; i++) {
        ;
    }
    while (i < LIST_SIZE) {
        head = (seg_list_ptr + i)->next;
        if (head == NULL)
        {
            i++;
            continue;
        }
    
        for (block = head;
            block != NULL 
            && get_size((block_t *)block) > 0 
            && !(get_alloc((block_t *)block));
            block = block->next)
        {
            unsigned long block_size = get_size((block_t *)block);

            if (asize <= block_size
                && !(get_alloc((block_t *)block)))
            {
                return (block_t *) block;
            }
        }
        i++;
    }

    dbg_ensures(mm_checkheap(__LINE__));
    return NULL; // no fit found
}

/*
 * Helper function for checking:
 * 1) payload aliconnecgnment
 * 2) minimum sizconnece
 * 3) header consconnecistency
 * 4) footer consconnecistency
 */
bool check_block(block_t *block)
{
    // Double-word alignment, lowest 4 bits are 0s
    int alignment_mask = 15;
    word_t *footer = header_to_footer(block);
    
    return (((uint64_t)block->payload) & alignment_mask) == 0
            && get_size(block) >= 2 * dsize
            && extract_alloc(*footer) == extract_alloc(block->header)
            && extract_size(*footer) == extract_size(block->header);
}

/* 
 * Heap Checker should run silently.
 * <What does this function do?>
 * <What are the function's arguments?>
 * <What is the function's return value?>
 * <Are there any preconditions or postconditions?>
 */
bool mm_checkheap(int line)
{

    traverse_free();

    /*
     * Implementation for heap checking
     * 1) Check epilogue and prologue blocks
     * 2) Check each block's address alignment
     * 3) Check heap boundaries
     * 4) Check each block's header and footer: size (minimum size, alignment)
     *    previous/next allocate/free bit consistency, header and footer matching
     *    each other
     * 5) Check coalescing, no two consecutive free blocks in the heap.
     */
    block_t *block_ptr = heap_start;  // points the payload of prologue
    bool prev_is_free = false;
    bool curr_is_free = false;

    for (block_ptr = heap_start; get_size(block_ptr) > 0; block_ptr = find_next(block_ptr))
    {   
        if (!check_block(block_ptr))
        {
            printf("Bad block.\n");

            // Debug mode toggled : Print out debugging info
            word_t *footer = header_to_footer(block_ptr);
            dbg_printf("Checkheap called failed from %d line.\n", line);
            dbg_printf("Block payload memory is %lu\n", (uint64_t)block_ptr->payload);
            dbg_printf("Header shows block size is %zu\n", extract_size(block_ptr->header));
            dbg_printf("Footer shows block size is %zu\n", extract_size(*footer));
            dbg_printf("Header shows block alloc is %d\n", get_alloc(block_ptr));
            dbg_printf("Footer shows block alloc is %d\n", extract_alloc(*footer));
            dbg_printf("Memory alignment is %d\n", (((uint64_t)block_ptr->payload) & 15) == 0);
            dbg_printf("\nExit...\n");
            exit(0);
        }
        // Checking for consecutive free blocks
        curr_is_free = !get_alloc(block_ptr);
        if (prev_is_free && curr_is_free)
        {
            dbg_printf("Consecutive free blocks found.\n");
            dbg_printf("\nExit...\n");
            exit(0);
        } else {
            prev_is_free = curr_is_free;
        }
    }

    // check heap epilogue, epilogue is a header
    if (get_size(block_ptr) != 0 || !get_alloc(block_ptr)) {
        dbg_printf("Checkheap FAILED: epilogue is bad.\n");
        exit(0);
    } else {
        //
    }
    return true;
}

/**
 * helper function to traverse the free list
 */
void traverse_free()
{
    dbg_printf("\nTraversing free list headers...\n\n");
    free_t *free_root;
    for (int i = 0; i < LIST_SIZE; i++) {
        free_root = (seg_list_ptr + i)->next;
        if (free_root == NULL) {
            dbg_printf("free root header at index %d is NULL, moving on...\n", i);
        } else {
            dbg_printf("free root header at index %d is NOT NULL...\n", i);
            free_t *block;
            for (block = free_root;
                block != NULL
                && get_size((block_t *)block) > 0;
                block = block->next)
            {
                dbg_printf("\n    next block address is %p\n", block);
                dbg_printf("    block with size %lu\n", get_size((block_t *)block));
                dbg_printf("    block alloc: %d\n", get_alloc((block_t *) block));
            }
        }
    }
}

/*
 *****************************************************************************
 * The functions below are short wrapper functions to perform                *
 * bit manipulation, pointer arithmetic, and other helper operations.        *
 *                                                                           *
 * We've given you the function header comments for the functions below      *
 * to help you understand how this baseline code works.                      *
 *                                                                           *
 * Note that these function header comments are short since the functions    *
 * they are describing are short as well; you will need to provide           *
 * adequate details within your header comments for the functions above!     *
 *                                                                           *
 *                                                                           *
 * Do not delete the following super-secret(tm) lines!                       *
 *                                                                           *
 * 53 6f 20 79 6f 75 27 72 65 20 74 72 79 69 6e 67 20 74 6f 20               *
 *                                                                           *
 * 66 69 67 75 72 65 20 6f 75 74 20 77 68 61 74 20 74 68 65 20               *
 * 68 65 78 61 64 65 63 69 6d 61 6c 20 64 69 67 69 74 73 20 64               *
 * 6f 2e 2e 2e 20 68 61 68 61 68 61 21 20 41 53 43 49 49 20 69               *
 *                                                                           *
 * 73 6e 27 74 20 74 68 65 20 72 69 67 68 74 20 65 6e 63 6f 64               *
 * 69 6e 67 21 20 4e 69 63 65 20 74 72 79 2c 20 74 68 6f 75 67               *
 * 68 21 20 2d 44 72 2e 20 45 76 69 6c 0a de ba c1 e1 52 13 0a               *
 *                                                                           *
 *****************************************************************************
 */

static size_t min(size_t x, size_t y)
{
    return (x < y) ? x : y;
}

/*
 * max: returns x if x > y, and y otherwise.
 */
static size_t max(size_t x, size_t y)
{
    return (x > y) ? x : y;
}


/*
 * round_up: Rounds size up to next multiple of n
 */
static size_t round_up(size_t size, size_t n)
{
    return n * ((size + (n-1)) / n);
}


/*
 * pack: returns a header reflecting a specified size and its alloc status.
 *       If the block is allocated, the lowest bit is set to 1, and 0 otherwise.
 */
static word_t pack(size_t size, bool alloc)
{
    return alloc ? (size | alloc_mask) : size;
}


/*
 * extract_size: returns the size of a given header value based on the header
 *               specification above.
 */
static size_t extract_size(word_t word)
{
    return (word & size_mask);
}


/*
 * get_size: returns the size of a given block by clearing the lowest 4 bits
 *           (as the heap is 16-byte aligned).
 */
static size_t get_size(block_t *block)
{
    return extract_size(block->header);
}


/*
 * get_payload_size: returns the payload size of a given block, equal to
 *                   the entire block size minus the header and footer sizes.
 */
static word_t get_payload_size(block_t *block)
{
    size_t asize = get_size(block);
    return asize - dsize;
}


/*
 * extract_alloc: returns the allocation status of a given header value based
 *                on the header specification above.
 */
static bool extract_alloc(word_t word)
{
    return (bool) (word & alloc_mask);
}


/*
 * get_alloc: returns true when the block is allocated based on the
 *            block header's lowest bit, and false otherwise.
 */
static bool get_alloc(block_t *block)
{
    return extract_alloc(block->header);
}


/*
 * write_header: given a block and its size and allocation status,
 *               writes an appropriate value to the block header.
 */
static void write_header(block_t *block, size_t size, bool alloc)
{
    dbg_requires(block != NULL);
    block->header = pack(size, alloc);
}


/*
 * write_footer: given a block and its size and allocation status,
 *               writes an appropriate value to the block footer by first
 *               computing the position of the footer.
 */
static void write_footer(block_t *block, size_t size, bool alloc)
{
    dbg_requires(block != NULL);
    dbg_requires(get_size(block) == size && size > 0);
    word_t *footerp = header_to_footer(block);
    *footerp = pack(size, alloc);
}

/*
 * find_next: returns the next consecutive block on the heap by adding the
 *            size of the block.
 */
static block_t *find_next(block_t *block)
{
    dbg_requires(block != NULL);
    dbg_requires(get_size(block) != 0);
    return (block_t *) ((char *) block + get_size(block));
}

/*
 * find_prev_footer: returns the footer of the previous block.
 */
static word_t *find_prev_footer(block_t *block)
{
    // Compute previous footer position as one word before the header
    return &(block->header) - 1;
}


/*
 * find_prev: returns the previous block position by checking the previous
 *            block's footer and calculating the start of the previous block
 *            based on its size.
 */
static block_t *find_prev(block_t *block)
{
    dbg_requires(block != NULL);
    dbg_requires(get_size(block) != 0);
    word_t *footerp = find_prev_footer(block);
    size_t size = extract_size(*footerp);
    return (block_t *) ((char *) block - size);
}


/*
 * payload_to_header: given a payload pointer, returns a pointer to the
 *                    corresponding block.
 */
static block_t *payload_to_header(void *bp)
{
    return (block_t *) ((char *) bp - offsetof(block_t, payload));
}


/*
 * header_to_payload: given a block pointer, returns a pointer to the
 *                    corresponding payload.
 */
static void *header_to_payload(block_t *block)
{
    return (void *) (block->payload);
}


/*
 * header_to_footer: given a block pointer, returns a pointer to the
 *                   corresponding footer.
 */
static word_t *header_to_footer(block_t *block)
{
    return (word_t *) (block->payload + get_size(block) - dsize);
}

/*
 * Helper function for rearranging the explicit free list.
 * Connects current free block's predecessor and successor together
 * Taking block out of this chain
 */
static void remove_from_free(free_t *free_b, int root_index)
{
    if (root_index == -1) {  // called from coalesce, doesn't know
        size_t s = extract_size(free_b->header);
        s >>= 3;
        int i;
        for (i = 0; s > seglist[i] && i < LIST_SIZE - 1; i++) {
            ;
        }
        root_index = i;
    }
    dbg_printf("free root address is %p\n", free_b);
    dbg_printf("next block address is %p\n", free_b->next);
    dbg_printf("prev block address is %p\n", free_b->prev);
    
    if (free_b->next == NULL && free_b->prev == NULL) {
        (seg_list_ptr + root_index)->next = NULL;
    } else {
        free_t *temp_prev = free_b->prev;
        free_t *temp_next = free_b->next;
        if (temp_next == NULL) {  // removal at the end
            temp_prev->next = NULL;
        } else if (temp_prev == NULL) {  // removal at the start
            (seg_list_ptr + root_index)->next = temp_next;
            temp_next->prev = NULL;
        } else {  // removal in the middle
            temp_prev->next = temp_next;
            temp_next->prev = temp_prev;
        }
    }
    dbg_printf("Removal terminated, checking heap\n");
}

/**
 * Helper function for inserting a free node into the appropriate segregated
 * free list. Traverse the size array to determine where to insert to.
 */
static void insert(free_t *free_b)
{
    int i;
    size_t s = extract_size(free_b->header);
    dbg_printf("\nFree block to be inserted : %lu\n", s);
    s >>= 3;
    
    for (i = 0; s > seglist[i] && i < LIST_SIZE - 1; i++) {
        ;
    }
    dbg_printf("Free block to be inserted at header %d\n\n", i);
    traverse_free();
    
    if ((seg_list_ptr + i)->next == NULL) {
        (seg_list_ptr + i)->next = free_b;
        free_b->next = NULL;
        free_b->prev = NULL;
    } else {
        // Address indexing policy
        free_t *old_root = (seg_list_ptr + i)->next;
        if (free_b < old_root) {
            (seg_list_ptr + i)->next = free_b;
            free_b->next = old_root;
            free_b->prev = NULL;
            old_root->prev = free_b;
        } else {
            // LIFO policy, code works with list header with only one block
            free_t *old_root = (seg_list_ptr + i)->next;
            (seg_list_ptr + i)->next = free_b;
            free_b->next = old_root;
            free_b->prev = NULL;
            old_root->prev = free_b;

            // while (old_root->next != NULL && free_b > old_root) {
            //     old_root = old_root->next;
            // }
            // free_t *temp = old_root->next;
            // old_root->next = free_b;
            // free_b->next = temp;
            // if (temp != NULL) temp->prev = free_b;
            // free_b->prev = old_root;
        }
    }
}